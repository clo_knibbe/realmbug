package com.example.clo.realmbug;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class MainActivityTests {

	@Rule
	public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

	/**
	 * This test passes when using Realm 3.0.0, but crashes with 3.1.0, 3.1.1, 3.1.2.
	 *
	 * The problem seems to be related to adding/removing the listener in
	 * MainActivity.onStart/onStop.  Commenting out the remove of the listener in onStop
	 * avoids the crash.
	 */
	@Test
	public void test() {
		// click on button to launch SecondActivity
		onView(withId(R.id.button)).perform(click());

		// verify in SecondActivity
		onView(withId(R.id.SecondActivityLayout)).check(matches(isDisplayed()));

		/*
		 * Pressing the back button causes crash:
		 * 		Test failed to run to completion.
		 * 		Reason: 'Instrumentation run failed due to 'Native crash''.
		 * 		Check device logcat for details
		 */

		// click on back button, to return to MainActivity
		pressBack();

		// verify in MainActivity
		onView(withId(R.id.MainActivityLayout)).check(matches(isDisplayed()));
	}
}
