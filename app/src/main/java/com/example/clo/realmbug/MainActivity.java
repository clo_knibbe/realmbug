package com.example.clo.realmbug;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmModel;
import io.realm.RealmObject;

public class MainActivity extends AppCompatActivity {

	private Realm realm;
	private RealmThing realmThing;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		realm = Realm.getDefaultInstance();

		realm.beginTransaction();
		realmThing = realm.createObject(RealmThing.class);
		realmThing.setValue(123);
		realm.commitTransaction();
	}

	@Override
	public void onStart() {
		super.onStart();
		realmThing.addChangeListener(new RealmChangeListener<RealmModel>() {
			@Override
			public void onChange(RealmModel element) {
				Log.d("MainActivity", "onChange: ");
			}
		});
	}

	@Override
	public void onStop() {
		super.onStop();
		realmThing.removeAllChangeListeners();
	}

	public void goToSecondActivity(View v) {
		Intent intent = new Intent(this, SecondActivity.class);
		startActivity(intent);
	}
}
