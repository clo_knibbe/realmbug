package com.example.clo.realmbug;

import io.realm.RealmObject;

public class RealmThing extends RealmObject {

	private int value;

	public RealmThing() {
	}

	public RealmThing(int value) {
		this.value = value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
